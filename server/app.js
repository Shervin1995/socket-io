var path = require('path') 
var express = require('express') 
var app = express() 
const io = require('socket.io')()


//
app.use(express.static('server/public')) 


//
app.get( '/' , (req, res) => {

  res.sendFile(path.resolve(__dirname + "public/index.html"));
  
});


//
var server = app.listen(3000)

//
io.attach(server);

//
io.on('connection', socket => {

  socket.on('postMessage', data => io.emit('updateMessages', data))

});



